package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.w3c.dom.Node;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.exceptions.TripNotFoundException;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;

import com.google.gson.*; 



public class STSManager<E> implements ISTSManager{
	
	private StopVO[] stops1;
	private BusUpdateVO[] busUpdates1;
	private IQueue<BusUpdateVO> busUpdates;
	private IStack<StopVO> stops;
	
	public STSManager() {
		stops = new Stack<StopVO>();
		busUpdates = new Queue<BusUpdateVO>();
	}

	public void readBusUpdate(File rtFile) {
		// TODO Auto-generated method stub
		 BufferedReader reader = null;
		 try {
			 reader = new BufferedReader(new FileReader(rtFile));
		     Gson gson = new GsonBuilder().create();

		     busUpdates1 = gson.fromJson(reader, BusUpdateVO[].class);
		     //System.out.println(busUpdates1.length);
		     for (int i = 0; i < busUpdates1.length; i++) {
				busUpdates.enqueue(busUpdates1[i]);
			}
		    
		} catch (Exception e) {
			// TODO: handle exception
		     System.out.println("payla");

		}
		 //System.out.println(busUpdates.darUltimo().getTripId()); 
		 //System.out.println(busUpdates.size());
	}
	
	/**
	  public static void main(String[] args) {
       
         JSONParser parser = new JSONParser();
 
       try {
 
           JSONArray listaGson = (JSONArray) parser.parse(new FileReader("data/BUSES_SERVICE_0.json"));
           Iterator iter = listaGson.iterator();
           int i = 0;
           while (iter.hasNext())
           {
            JSONObject obj = (JSONObject) listaGson.get(i);
            
            //String vehicleNo = (String) miau.parse("vehicleNo");
           String tripId = miau.get("VehicleNo").toString();
            String RouteNo = miau.get("RouteNo").toString();
           
           System.out.println("VehicleNo: " + tripId);
           System.out.println("RouteNo: " + RouteNo);
            i++;
           iter.next();
           }

 
       } catch (Exception e) {
           e.printStackTrace();
       }
   }
	 */
	
	public IQueue<BusUpdateVO> listTrips(Integer tripId){
		IQueue<BusUpdateVO> listUpdates = new Queue<BusUpdateVO>();
		while (busUpdates.hasNext()) {
			if (busUpdates.darPrimero().getTripId()==tripId) {
				listUpdates.enqueue(busUpdates.darPrimero());
				try {
					busUpdates.dequeue();
				} catch (TripNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else try {
				busUpdates.dequeue();
			} catch (TripNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		System.out.println("size "+listUpdates.size()); 
		return listUpdates;
		//9135629
	}

	public IStack<StopVO> listStops(Integer tripID) throws TripNotFoundException {
		IStack<StopVO> listStops = new Stack<StopVO>();
		//System.out.println("entro");
		IQueue<BusUpdateVO> listTrips = (IQueue<BusUpdateVO>) listTrips(tripID);
		//System.out.println("entro2");
		while (listTrips.hasNext()) {
			double lat1 = listTrips.darPrimero().getLatitude();
			double long1 = listTrips.darPrimero().getLongitude();
			double lat2=0;
			double long2=0;
			while (stops.hasNext()) {
				lat2 = stops.darPrimero().getLat();
				long2 = stops.darPrimero().getLong();
				System.out.println(getDistance(lat1, long1, lat2, long2));
				if (getDistance(lat1, long1, lat2, long2) <= 70) {
					listStops.push(stops.darPrimero());
					System.out.println(stops.darPrimero().getName());
					try {
						stops.pop();
					} catch (TripNotFoundException e) {
						// TODO Auto-generated catch block
						System.out.println("quepaso1");
						//e.printStackTrace();
					}
				}
				else try {
					stops.pop();
				} catch (TripNotFoundException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					System.out.println("quepaso2");
				}
				//System.out.println(stops.size());
				
			}
			try {
				listTrips.dequeue();
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("quepaso");
			}
			//System.out.println(listTrips.size());
		}
			
		if (listStops.size()==0) {
			throw new TripNotFoundException();
		}
		
		return listStops;
	}
	
	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
        // TODO Auto-generated method stub
        final int R = 6371*1000; // Radious of the earth
    
        Double latDistance = toRad(lat2-lat1);
        Double lonDistance = toRad(lon2-lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
                   Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
                   Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double distance = R * c;
        
        return distance;
 
    }

  private Double toRad(Double value) {
        return value * Math.PI / 180;
    }


	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( stopsFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					String ID = partes[0];
					int stopID = Integer.parseInt(ID);
					String stopCode = partes[1];
					String stopName = partes[2];
					String stopDesc = partes[3];
					String stopLat = partes[4];
					String stopLon = partes[5];
					String zoneID = partes[6];
					String stopURL = partes[7];
					String locationType = partes[8];
					String parentStation = "";
					if (partes.length > 9) {
						parentStation = partes[9];
					}
					
					StopVO stop = new StopVO(stopID,stopCode,stopName,stopDesc,stopLat,stopLon,zoneID,stopURL,locationType,parentStation);
					stops.push(stop);
					System.out.println(stops.darPrimero().getName());
					linea = lector.readLine( );
				}
				lector.close( );
			}
			catch( IOException e )
			{
				System.out.print("que pazo");
			}
		}
	}



}
