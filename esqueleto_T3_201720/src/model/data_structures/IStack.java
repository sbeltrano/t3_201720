package model.data_structures;

import model.exceptions.TripNotFoundException;

public interface IStack<E> {

	public void push (E item);
	
	public E pop() throws TripNotFoundException;
	
	public int size();
	
	public E darPrimero();
	
	public E darActual();
	
	public void cambiarASig();
	
	public boolean hasNext();
}
