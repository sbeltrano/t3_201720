package model.data_structures;

import java.util.Iterator;

import model.exceptions.TripNotFoundException;

public interface IQueue<E> {
	
	public void enqueue(E item);
	
	public E dequeue() throws TripNotFoundException;
	
	public int size();
	
	public E darPrimero();
	
	public E darUltimo();
	
	public E darActual();
	
	public void cambiarASig();
	
	public Iterator<E> iterator();
	
	public boolean hasNext();

}
