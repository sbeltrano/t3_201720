package model.data_structures;

import model.data_structures.Queue.Node;
import model.exceptions.TripNotFoundException;

public class Stack<E> implements IStack<E> {
	private Node<E> first;     // top of stack
    private int size;      
    private Node<E> actual;// size of the stack

    // helper linked list class
    public static class Node<E> {
        private E item;
        private Node<E> next;
    }
    
    public Stack() {
        first = null;
        actual = null;
        size = 0;
    }
    
    public boolean hasNext(){
    	return first.next!=null;
    }

    public boolean isEmpty() {
        return first == null;
    }
    
    public int size() {
        return size;
    }
    
    public E darPrimero() {
    	E e = first.item;
		return e;
		
	}
    
    public E darActual(){
    	E e = actual.item;
    	return e;
    }
    
    public void cambiarASig(){
    	actual = actual.next;
    }
    
	public void push(E item) {
		// TODO Auto-generated method stub
		Node<E> oldfirst = first;
        first = new Node<E>();
        first.item = item;
        first.next = oldfirst;
        size++;
	}

	public E pop() throws TripNotFoundException{
		if (isEmpty()) throw new TripNotFoundException();
        E item = first.item;        // save item to return
        first = first.next;            // delete first node
        size--;
        return item;    
	}
}
